## Project MISR SOM data
# Use with un-georeferenced, atm-corrected dat files output by Surf.
# Hard coded for 512*2048 blocks and expects specific filenames too.

# Call: Rscript project_misr.R <filename without path> <orbit path nnn> <block nnn>
# For local debugging, open an R shell and specify the args variable manually, then source script
# Specify: fname_vals orbit_path block product new_proj

L0DIR = Sys.getenv('L0DIR')
PROCESS_DIR = Sys.getenv('PROCESS_DIR')

library(raster)

system(paste0('mkdir -p ', PROCESS_DIR, 'Rdump'))
rasterOptions(tmpdir=paste0(PROCESS_DIR, 'Rdump'))

# Identify input file characteristics
if(product == 'MIRCCM'){
	fn_lats = paste(PROCESS_DIR, 'grids/lat_p', orbit_path, '_b', block, '_1100m.raw', sep='')
	fn_lons = paste(PROCESS_DIR, 'grids/lon_p', orbit_path, '_b', block, '_1100m.raw', sep='')
	fn_vals = paste(PROCESS_DIR, 'cloud_masks_binary/', fname_vals, sep='')
	n = 128 * 512
	iterp_method = 'ngb'
	resolution = 1100
} else {
	fn_lats = paste(PROCESS_DIR, 'grids/lat_p', orbit_path, '_b', block, '.raw', sep='')	
	fn_lons = paste(PROCESS_DIR, 'grids/lon_p', orbit_path, '_b', block, '.raw', sep='')
	fn_vals = paste(PROCESS_DIR, 'Surf/', fname_vals, sep='')
	n = 512 * 2048
	iterp_method = 'bilinear'
	resolution = 275
}

fh_lats = file(fn_lats, 'rb')
lats = readBin(fh_lats, double(), n=n, endian='little', size=8)

fh_lons = file(fn_lons, 'rb')
lons = readBin(fh_lons, double(), n=n, endian='little', size=8)

# Values

fh_vals = file(fn_vals, 'rb')
vals = readBin(fh_vals, double(), n=n, endian='little')


# Create lon, lat, runoff
xyz = cbind(lons, lats, vals)
colnames(xyz) = c('lon', 'lat', 'val')
xyz = as.data.frame(xyz)
# x and y here are the column headings contained in xyz
coordinates(xyz) = ~lon+lat
crs(xyz) = '+init=epsg:4326'
# Remove null pixels, no point rasterizing these
xyz = xyz[xyz$val > -999999,]

# Project coordinates to new projection
xyz_new_proj = spTransform(xyz, CRS(new_proj))

# Create empty raster with same res as input resolution
rast = raster(ext=extent(xyz_new_proj), crs=new_proj, resolution=resolution)
# Put latlon irregular points onto pstere raster
rasOut = rasterize(xyz_new_proj, rast, xyz_new_proj$val)
# Interpolate to 300 m
interp_grid = raster(ext=extent(xyz_new_proj), crs=new_proj, resolution=300)
interpolated = resample(rasOut, interp_grid, method=iterp_method)

# Define function to fill nodata gaps
# https://gis.stackexchange.com/questions/181011/fill-the-gaps-using-nearest-neighbors
fill.na = function(x, i=5) {
  if( is.na(x)[i] ) {
    return( round(mean(x, na.rm=TRUE),0) )
  } else {
    return( round(x[i],0) )
  }
}  

# Fill gaps in nearest-neighbour products
if(iterp_method == 'ngb'){
	interpolated = focal(interpolated, w=matrix(1,5,5), fun=fill.na, pad=TRUE, na.rm=FALSE)
}
interpolated[is.na(interpolated)] = -9999

# Save to GeoTIFF
if(product == 'MIRCCM'){
	fname = paste(PROCESS_DIR, 'cloud_masks_projected/', fname_vals, '.tif', sep='')
} else {
	fname = paste(PROCESS_DIR, 'projected/', fname_vals, '.tif', sep='')	
}

writeRaster(interpolated, fname, overwrite=TRUE, NAflag=-9999)


