"""
Helper functions for working with MISR data.

Requires the MisrToolkit C functions to be available in the shell environment.

Andrew Tedstone, June 2017
"""

import subprocess
import re
from shapely import geometry
import geopandas as gpd
import numpy as np

import MisrToolkit as mtk



def block_intersects_poly(path, block, polygon):
	"""
	Check whether a path-block combination intersects with a polygon.

	:param path: MISR path number
	:type path: int
	:param block: MISR block number within the specified path
	:type block: int
	:param polygon: a shapely.geometry.Polygon object in WGS84 lat/lon.

	:returns: True if the block intersects the polygon.
	:rtype: bool

	"""

	block_corners = mtk.path_block_range_to_block_corners(int(path), int(block), int(block))
	tb = block_corners.block[block]
	points = ((tb.llc.lon, tb.llc.lat), (tb.ulc.lon, tb.ulc.lat), 
		(tb.urc.lon, tb.urc.lat), (tb.lrc.lon, tb.lrc.lat))

	# Convert to shapely polygon
	path_block = geometry.Polygon(points)

	# Does path-block intersect with the polygon provided?
	if path_block.intersects(polygon):
		return True
	else:
		return False



def block_perc_cloudy(filename, path, block):
	"""

	Page 85 of MISR Data Products Specifications:
	0 = no retrieval
	1 = cloud, high confidence
	2 = cloud, low confidence
	3 = clear, low confidence
	4 = clear, high confidence
	255 = fill

	"""

	f = mtk.MtkFile(filename)
	region = mtk.MtkRegion(path, block, block)
	data = f.grid('RCCM').field('Cloud').read(region).data()

	totpx = data.shape[0] * data.shape[1]
	clearpx = np.sum(np.where(data <= 2, 1, 0))
	perc_cloudy = (100. / totpx) * clearpx

	return perc_cloudy


