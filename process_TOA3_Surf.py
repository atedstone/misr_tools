"""
Process MISR data through TOA3 and Surf

Remember to set L0DIR and PROCESS_DIR

Usage:
	$ python process_TOA3_Surf.py MI1B2T_filelist.txt

Filename layouts:

MI1B2T:
MISR_AM1_GRP_TERRAIN_GM_Pnnn_Onnnnn_CC_F03_0024.hdf

MIB2GEOP:
MISR_AM1_GP_GMP_Pnnn_Onnnnn_F03_0013.hdf

Andrew Tedstone, June 2017
"""

import subprocess
import os
import sys
import numpy as np
import functools
import multiprocessing
import configparser
import geopandas as gpd
import traceback
import MisrToolkit as mtk
#import shapefile
import misr_functions

debug = False

# Load configuration parameters
config = configparser.ConfigParser()
config.read_file(open(sys.argv[1]))

pconfig = {}
pconfig['band'] = config.get('Params', 'band').encode('ascii', 'ignore')
pconfig['minnaert'] = config.get('Params', 'minnaert').encode('ascii', 'ignore')
pconfig['region'] = config.get('Params', 'region').encode('ascii', 'ignore')
pconfig['aoi_shp'] = config.get('Params', 'aoi_shp').encode('ascii', 'ignore')
cameras = config.get('Params', 'cameras').encode('ascii', 'ignore').split(',')
pconfig['cameras'] = [c.strip() for c in cameras]
pconfig['MI1B2T_ver'] = config.get('Params', 'MI1B2T_ver').encode('ascii', 'ignore')
pconfig['MIB2GEOP_ver'] = config.get('Params', 'MIB2GEOP_ver').encode('ascii', 'ignore')
pconfig['MIRCCM_ver'] = config.get('Params', 'MIRCCM_ver').encode('ascii', 'ignore')
cloudy = config.get('Params', 'cloudy')
if cloudy.lower() == 'none': 
	pconfig['cloudy'] = None
else:
	pconfig['cloudy'] = cloudy
pconfig['cloud_masks'] = bool(config.get('Params', 'cloud_masks').encode('ascii', 'ignore'))
pconfig['to_proj4'] = config.get('Params', 'to_proj4')

# Load environment variables
pconfig['L0DIR'] = os.environ['L0DIR']
pconfig['PROCESS_DIR'] = os.environ['PROCESS_DIR']
pconfig['OSU_DIR'] = os.environ['OSU_DIR']
pconfig['DEVDIR'] = os.environ['DEVDIR']


# Open list of granules to process
listfile = open(sys.argv[2], 'r')
to_process = []
for line in listfile:
	to_process.append(line.strip())


# Load area of interest polygon
aoi_poly = None
if pconfig['aoi_shp'] != 'None':
	# Re do to use shapefile class at a later date...
	# In the meantime this is hardcoded to definitely work for GrIS, elsewhere unknown
	#shp = shapefile.Reader(aoi_shp)
	shp = gpd.read_file(pconfig['aoi_shp'])
	aoi_poly = shp.iloc[0].geometry 
pconfig['aoi_shp_geom'] = aoi_poly


def process_block(granule, fn_MIB2GEOP, granule_path, granule_orbit, pc, block):
	"""
	Process a single block of data
	Map to this function across a multiprocessing pool
	"""


	# First check cloud cover (if requested)
	# Just use the cloud mask of the first camera in the list
	if pc['cloudy'] is not None:
		CCM_fn = 'MISR_AM1_GRP_RCCM_GM_P%s_O%s_%s_%s.hdf' %(granule_path, granule_orbit, pc['cameras'][0], pc['MIRCCM_ver'])
		CCM_path = pc['L0DIR'] + 'MIRCCM/' + CCM_fn
		cloud_perc = misr_functions.block_perc_cloudy(CCM_path, int(granule_path), int(block))
		if cloud_perc > int(pc['cloudy']):
			print('Rejected %s:%s on cloud cover (%.0f perc)' %(granule, block, cloud_perc))
			return

	for camera in pc['cameras']:

		# Do TOA3
		B2T_fn = '%s_%s_%s.hdf' %(granule, camera, pc['MI1B2T_ver'])
		B2T_path = pc['L0DIR'] + 'MI1B2T/' + B2T_fn
		out_fn_generic = '%s_%s_%s_B%s' %(granule, camera, pc['MI1B2T_ver'], str(block))
		# For some reason Surf requires lower-case filename parameters on input file
		TOA3_out_dat = pc['PROCESS_DIR'] + 'TOA3/' + out_fn_generic.lower() + '.dat'
		TOA3_out_png = pc['PROCESS_DIR'] + 'TOA3/' + out_fn_generic + '.png'
		# TOA3 input-misr-file block band minnaert output-data-file output-image-file
		cmd = './TOA3 %s %s %s %s %s %s' %(B2T_path, block, pc['band'], pc['minnaert'], TOA3_out_dat, TOA3_out_png)
		print(cmd)
		subprocess.call(cmd, shell=True)

		# Check to see if block is actually available
		if not os.path.isfile(TOA3_out_dat):
			print('Block not available, skipping')
			return

		# Process to surface reflectance
		surf_out_dat = pc['PROCESS_DIR'] + 'Surf/' + out_fn_generic + '.dat'
		surf_out_png = pc['PROCESS_DIR'] + 'Surf/' + out_fn_generic + '.png'
		# Surf toa-data-file gmp-file band output-data-file output-image-file
		cmd = './Surf %s %s %s %s %s' %(TOA3_out_dat, fn_MIB2GEOP, pc['band'], surf_out_dat, surf_out_png)
		print(cmd)
		subprocess.call(cmd, shell=True)

		# Project using R
		# Rscript project_misr.R <filename without path> <orbit path nnn> <block nnn>
		cmd = 'Rscript %s/cmd_project_misr.R %s %s %s %s %s' %(pc['DEVDIR'], surf_out_dat.split('/')[-1], int(granule_path), block, 'MI1B2T', pc['to_proj4'])
		print(cmd)
		subprocess.call(cmd, shell=True)


	if pc['cloud_masks']:
		# Cloud masks are only 1.1 x 1.1km resolution
		# Aggregate to a single cloud mask
		merge = np.zeros((128, 512))
		for camera in pc['cameras']:
			CCM_fn = 'MISR_AM1_GRP_RCCM_GM_P%s_O%s_%s_%s.hdf' %(granule_path, granule_orbit, camera, pc['MIRCCM_ver'])
			CCM_path = pc['L0DIR'] + 'MIRCCM/' + CCM_fn
			f = mtk.MtkFile(CCM_path)
			region = mtk.MtkRegion(int(granule_path), int(block), int(block))
			data = f.grid('RCCM').field('Cloud').read(region).data()
			# 1= only pick 'clouds with high confidence'
			cloudy = np.where(data == 1, 1, 0)
			merge = np.where((cloudy == 1) | (merge == 1), 1, 0)
		CCM_combo_fn = 'MISR_AM1_GRP_RCCM_GM_P%s_O%s_B%s_%s.dat' %(granule_path, granule_orbit, block, pc['MIRCCM_ver'])
		CCM_combo_path = pc['PROCESS_DIR'] + 'cloud_masks_binary/' + CCM_combo_fn
		merge.astype('float').tofile(CCM_combo_path)

		# Now project them
		cmd = 'Rscript %s/cmd_project_misr.R %s %s %s %s %s' %(pc['DEVDIR'], CCM_combo_fn, int(granule_path), block, 'MIRCCM', pc['to_proj4'])
		print(cmd)
		subprocess.call(cmd, shell=True)

	return



# We need to be in OSU directory so that the scripts can see their auxillary files
os.chdir(pconfig['OSU_DIR'])

for granule in to_process:

	granule = granule.split('/')[-1]
	print(granule)

	granule_path = granule.split('_P')[1][0:3]
	granule_orbit = granule.split('_O')[1][0:6]

	fn_MIB2GEOP = pconfig['L0DIR'] + 'MIB2GEOP/MISR_AM1_GP_GMP_P%s_O%s_%s.hdf' %(granule_path, granule_orbit, pconfig['MIB2GEOP_ver'])

	# This function doesn't work properly in current Python bindings, so use cmd line version
	cmd = 'MtkRegionPathToBlockRange --path=%s --setregion-ulclrc=%s' %(int(granule_path), pconfig['region'])
	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
	(stdout, stderr) = p.communicate()
	p.wait()

	# Generate block list
	blocks_range = [int(n) for n in stdout.strip().split('\n')]
	# +1 to make inclusive of last block
	blocks_list = np.arange(blocks_range[0], blocks_range[1]+1)

	# Cut block list down by checking intersection of each block with a polygon
	if pconfig['aoi_shp'] != 'None':
		aoi_blocks_list = []
		for block in blocks_list:
			if misr_functions.block_intersects_poly(granule_path, block, pconfig['aoi_shp_geom']):
				aoi_blocks_list.append(block)
		print('All blocks: %s, Blocks in AOI: %s' %(len(blocks_list), len(aoi_blocks_list)))
	else:
		aoi_blocks_list = blocks_list

	if len(aoi_blocks_list) == 0:
		continue

	# Submit to multi-processing pool
	if debug == False:
		try:
			proc_pool = multiprocessing.Pool(int(os.environ['OMP_NUM_THREADS']))
			func = functools.partial(process_block, granule, fn_MIB2GEOP, granule_path, granule_orbit, pconfig)
			res = proc_pool.map(func, aoi_blocks_list)
		except:
			raise Exception("".join(traceback.format_exception(*sys.exc_info())))
		finally:
			proc_pool.close()
			proc_pool.join()
	
	elif debug == True:
		# For debugging...
		for n in aoi_blocks_list:
			process_block(granule, fn_MIB2GEOP, granule_path, granule_orbit, pconfig, n)
